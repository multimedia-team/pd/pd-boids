lib.name = boids
class.sources = boids2d.c boids3d.c 
datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
